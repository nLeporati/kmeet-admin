import React from 'react';
import { Form, Icon, Input, Button, Checkbox, Row, Col, Typography } from 'antd';

class SignIn extends React.Component {
  state = {
    username: '',
    password: ''
  }
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  }
  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    
  };
  render() {
    return (
      <Row justify="center">
        <Col push={9} pull={9}>
          <Typography.Title>Login</Typography.Title>
          <Form onSubmit={this.handleSubmit} className="login-form">
            <Form.Item>
              <Input
                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                id="username"
                placeholder="Username"
                onChange={this.handleChange}
              />
            </Form.Item>
            <Form.Item>
              <Input
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                id="password"
                type="password"
                placeholder="Password"
                onChange={this.handleChange}
              />
            </Form.Item>
            <Form.Item>
              <Checkbox>Remember me</Checkbox>
              <a className="login-form-forgot" href="/">
                Forgot password
              </a>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
              {/* Or <a href="">register now!</a> */}
            </Form.Item>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default SignIn;