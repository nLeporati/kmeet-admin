import React, { Component } from 'react';
import { Typography, Row, Col } from 'antd';


const { Title } = Typography;

class Dashboard extends Component {
  state = {}
  render() {
    return (
      <Row>
        <Col>
          <Title>Dashboard</Title>
        </Col>
        <Col lg={16}>
          <Title level={3}>News</Title>
        </Col>
        <Col lg={8}>
          <Title level={3}>Notifications</Title>
        </Col>
      </Row>
    );
  }
}

export default Dashboard;