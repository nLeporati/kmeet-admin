import React from 'react'
import { Typography, Menu } from 'antd';
import { Link, NavLink, withRouter } from 'react-router-dom';

const { Title } = Typography;

const Navbar = () => {
  return (
    <div>
      <Menu theme="dark" defaultSelectedKeys={['home']} mode="horizontal" style={{ lineHeight: '64px' }}>
        <Menu.Item key="home">
          <NavLink to="/">Home</NavLink>
        </Menu.Item>
        <Menu.Item key="guides">
          <NavLink to="/guides">Guides</NavLink>
        </Menu.Item>
        <Menu.Item key="signin">
          <NavLink to="/signin">Login</NavLink>
        </Menu.Item>
      </Menu>
    </div>
  )
}

export default withRouter(Navbar);