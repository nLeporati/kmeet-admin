import React from 'react';
import { Row, Col, Typography, Button } from 'antd';
import GuideSummary from './GuideSummary';

const { Title } = Typography;

const GuideList = (props) => {
  const goAddGuide = () => {
    console.log(props);
    props.history.push('/guides/new');
  }
  return(
    <Row>
      <Col>
        <Title>Guide list</Title>
      </Col>
      <Col>
        <Button size="small" onClick={goAddGuide}>Add a new guide</Button> <br/>
      </Col>
      <GuideSummary></GuideSummary>
      <GuideSummary></GuideSummary>
      <GuideSummary></GuideSummary>
      <GuideSummary></GuideSummary>
      <GuideSummary></GuideSummary>
    </Row>
  )
}

export default GuideList;