import React, { Component } from 'react';
import { Form, Input, Button, Row, Col, Typography } from 'antd';

class GuideAdd extends Component {
  state = {
    name: '',
    rut: '',
    birthDate: '',
    // country: '',
    // region: '',
    // city: '',
    // direction: '',
  }
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  }
  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    
  };
  render() {
    return (
      <Row justify="center">
        <Col lg={8}>
          <Typography.Title>Add a new guide</Typography.Title>
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              <label>Nombre</label>
              <Input id="name" onChange={this.handleChange} />
            </Form.Item>
            <Form.Item>
            <label>Rut</label>
              <Input id="rut" onChange={this.handleChange} />
            </Form.Item>
            <Form.Item>
              <label>Fecha de nacimiento</label>
              <Input id="birthDate" onChange={this.handleChange} />
            </Form.Item>
            {/* <Col lg={12}>
              <Form.Item>
                <Input id="country" onChange={this.handleChange} />
              </Form.Item>
              <Form.Item>
                <Input id="region" onChange={this.handleChange} />
              </Form.Item>
              <Form.Item>
                <Input id="city" onChange={this.handleChange} />
              </Form.Item>
              <Form.Item>
                <Input id="direction" onChange={this.handleChange} />
              </Form.Item>
            </Col> */}
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Add
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default GuideAdd;