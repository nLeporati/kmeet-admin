import React from 'react';
import { Col, Row } from 'antd';

const GuideSummary = () => {
  return(
    <Row>
      <Col>
        <p>David Perdero</p>
      </Col>
    </Row>
  )
}

export default GuideSummary;