import React, { Component } from 'react';
import './App.css';
import { Layout  } from 'antd';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Components
import Navbar from './components/layout/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import GuideList from './components/guides/GuideList';
import GuideDetails from './components/guides/GuideDetails';
import SignIn from './components/auth/SingIn';
import GuideAdd from './components/guides/GuideAdd';

const { Header, Content, Footer } = Layout;

export default class App extends Component {
  render() {
    return(
      <BrowserRouter>
        <Layout className="layout">

          <Header>
            <Navbar></Navbar>
          </Header>

          <Content style={{ padding: '50px 50px' }}>
            <div style={{ background: '#fff', padding: 24, minHeight: 430 }}>
              <Switch>
                <Route exact path="/" component={Dashboard}></Route>
                <Route exact path="/guides" component={GuideList}></Route>
                <Route path="/guides/new" component={GuideAdd}></Route>
                <Route exact path="/guides/:id" component={GuideDetails}></Route>
                <Route exact path="/signin" component={SignIn}></Route>
              </Switch>
            </div>
          </Content>

          <Footer style={{ textAlign: 'center' }}>
            Kmeet Admin ©2019 Created by Nicolás Leporati
          </Footer>

        </Layout>
      </BrowserRouter>
    )
  }
}
